﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LigaMistru
{
    public partial class PridaniHrace : Form
    {
        public Hrac hrac { get; set; }
        public PridaniHrace(Hrac hrac)
        {
            InitializeComponent();
            foreach (FotbalovyKlub item in FotbalovyKlubInfo.ZiskejKluby())
            {
                comboBoxKlub.Items.Add(FotbalovyKlubInfo.DejNazev(item));
            }

            comboBoxKlub.SelectedIndex = 0;
            if (hrac != null)
            {
                textBoxJmeno.Text = hrac.Jmeno;
                comboBoxKlub.SelectedItem = FotbalovyKlubInfo.DejNazev(hrac.Klub);
                textBoxGoly.Text = hrac.GolPocet.ToString();
            }

        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            int result = 0;
            if (textBoxJmeno.Text != "" && int.TryParse(textBoxGoly.Text, out result))
            {
                Hrac hrac = new Hrac(textBoxJmeno.Text, FotbalovyKlubInfo.ZiskejKluby()[comboBoxKlub.SelectedIndex]);
                hrac.GolPocet = result;
                this.hrac = hrac;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Chybné zadání, opakujte prosím!", "Chyba vstupu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonStorno_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
