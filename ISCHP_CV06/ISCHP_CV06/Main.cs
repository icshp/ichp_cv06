﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LigaMistru
{
    public partial class Main : Form
    {
        Hraci hraci;

        public Main()
        {
            InitializeComponent();
            hraci = new Hraci();
            buttonRegistrovat_Click(null, null);
        }

        private void buttonPridejHrace_Click(object sender, EventArgs e)
        {
            PridaniHrace pridejHrace = new PridaniHrace(null);
            pridejHrace.ShowDialog();
            if (pridejHrace.DialogResult == DialogResult.OK)
            {
                hraci.Pridej(pridejHrace.hrac);
            }
        }

        private void buttonNejKlub_Click(object sender, EventArgs e)
        {
            Score score = new Score(hraci);
            score.Show();
        }

        private void buttonVymaz_Click(object sender, EventArgs e)
        {
            hraci.Vymaz((Hrac)listBoxHraci.SelectedItem);
        }

        private void buttonUprav_Click(object sender, EventArgs e)
        {
            if ((Hrac)listBoxHraci.SelectedItem != null)
            {
                PridaniHrace pridejHrace = new PridaniHrace((Hrac)listBoxHraci.SelectedItem);
                pridejHrace.ShowDialog();
                if (pridejHrace.DialogResult == DialogResult.OK)
                {
                    hraci.Vymaz((Hrac)listBoxHraci.SelectedItem);
                    hraci.Pridej(pridejHrace.hrac);
                    MessageBox.Show("Byl upraven: " + pridejHrace.hrac.ToString(), "Informace", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void buttonKonec_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonRegistrovat_Click(object sender, EventArgs e)
        {
            labelState.Text = "Zapnutý. Handler registrován!";
            labelState.ForeColor = Color.Green;
            hraci.UpdatedCount += PocetZmenen;
        }

        private void buttonZrusit_Click(object sender, EventArgs e)
        {
            labelState.Text = "Vypnutý. Handler neregistrován!";
            labelState.ForeColor = Color.Red;
            hraci.UpdatedCount -= PocetZmenen;
        }

        private void PocetZmenen(object sender, EventArgs e)
        {
            listBoxHraci.Items.Clear();
            for (int i = 0; i < hraci.Pocet; i++)
            {
                listBoxHraci.Items.Add(hraci[i]);
            }
            listBoxPocetHracu.Items.Add("Změna počtu hráčů: " + hraci.Pocet);
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                VybraniKlubu fk = new VybraniKlubu(FotbalovyKlubInfo.ZiskejKluby());
                if (fk.ShowDialog() == DialogResult.OK)
                {
                    if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        hraci.ulozHrace(saveFileDialog.FileName, fk.zvoleneKluby);
                    }
                }
                else {
                    MessageBox.Show("Nebyl vybrán žádný klub k exportu!");
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Chyba při ukládání dat!" + ex.ToString());
            }
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            try
            {
                if (saveFileDialog.FileName != null)
                    openFileDialog.FileName = saveFileDialog.FileName;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    hraci.nactiHrace(openFileDialog.FileName);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Požadovaný soubor je poškozený! Zvolte prosím jiný.");
            }      
        }
    }
}
