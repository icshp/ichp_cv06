﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LigaMistru
{
    public enum FotbalovyKlub
    {
        None,
        FCPorto,
        Arsenal,
        RealMadrid,
        Chelsea,
        Barcelona
    }

    public static class FotbalovyKlubInfo
    {
        public static readonly int Pocet = Enum.GetValues(typeof(FotbalovyKlub)).Length;

        public static string DejNazev(FotbalovyKlub fotbalovyKlub)
        {
            switch (fotbalovyKlub)
            {
                case FotbalovyKlub.None:
                    return "Nemá klub";
                case FotbalovyKlub.FCPorto:
                    return "FC Porto";
                case FotbalovyKlub.RealMadrid:
                    return "Real Madrid";
                default:
                    return fotbalovyKlub.ToString();
            }

            throw new Exception("Neplatná volba");
        }

        public static FotbalovyKlub DejEnum(string nazev)
        {
            Enum.TryParse(nazev, out FotbalovyKlub klub);
            return klub;
        }

        public static FotbalovyKlub[] ZiskejKluby()
        {
            return new FotbalovyKlub[] {
            FotbalovyKlub.None,
            FotbalovyKlub.FCPorto,
            FotbalovyKlub.Arsenal,
            FotbalovyKlub.RealMadrid,
            FotbalovyKlub.Chelsea,
            FotbalovyKlub.Barcelona};
        }
    }


}
