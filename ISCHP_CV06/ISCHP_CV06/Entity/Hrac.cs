﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LigaMistru
{
    public class Hrac
    {
        public string Jmeno { get; set; }
        public FotbalovyKlub Klub { get; set; }
        public int GolPocet { get; set; }


        public Hrac(string jmeno, FotbalovyKlub klub)
        {
            Jmeno = jmeno;
            Klub = klub;
            GolPocet = 0;
        }

        public Hrac() { 
        }

        public override string ToString()
        {
            return $"{Jmeno} (klub: {Klub}, golu: {GolPocet})";
        }

        public override bool Equals(Object obj)
        {
            return this.ToString() == obj.ToString();
        }
    }
}
