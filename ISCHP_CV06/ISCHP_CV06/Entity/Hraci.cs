﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LigaMistru
{
    public delegate void UpdatedCountEventHandler(object sender, EventArgs e);

    public class Hraci
    {
        public event UpdatedCountEventHandler UpdatedCount;
        private SpojovySeznam spojovySeznam;


        public void OnUpdatedCount()
        {
            UpdatedCountEventHandler handler = UpdatedCount;
            if (handler != null)
                handler(this, new EventArgs());
        }
        public Hraci()
        {
            spojovySeznam = new SpojovySeznam();
        }

        public int Pocet
        {
            get => spojovySeznam.Count;
        }

        public Hrac this[int index]
        {
            get => (LigaMistru.Hrac)spojovySeznam[index];
        }

        public void Vymaz(Hrac hrac)
        {
            spojovySeznam.Remove(hrac);
            OnUpdatedCount();
        }

        public void Pridej(Hrac hrac)
        {
            if (hrac != null)
            {
                spojovySeznam.Add(hrac);
                OnUpdatedCount();
            }
        }

        public void NajdiNejlepsiKluby(out FotbalovyKlub[] kluby, out int pocetGolu)
        {
            Dictionary<FotbalovyKlub, int> score = new Dictionary<FotbalovyKlub, int>();
            foreach (FotbalovyKlub item in FotbalovyKlubInfo.ZiskejKluby())
            {
                score.Add(item, 0);
            }

            spojovySeznam.Reset();
            foreach (Hrac item in spojovySeznam)
            {
                score[item.Klub] += item.GolPocet;
            }

            pocetGolu = score.Values.Max();
            List<FotbalovyKlub> maxKluby = new List<FotbalovyKlub>();
            if (pocetGolu > 0)
            {
                foreach (var item in score)
                {
                    if (item.Value == pocetGolu)
                    {
                        maxKluby.Add(item.Key);
                    }
                }
            }
            kluby = maxKluby.ToArray();
        }

        public void ulozHrace(string cesta, List<FotbalovyKlub> kUlozeni)
        {
            using (StreamWriter sw = new StreamWriter(cesta))
            {
                spojovySeznam.Reset();
                foreach (Hrac hrac in spojovySeznam)
                {
                    if(kUlozeni.Contains(hrac.Klub))
                        sw.WriteLine(hrac.Jmeno + " " + hrac.Klub + " " + hrac.GolPocet);
                }
            }
        }

        public void nactiHrace(string cesta)
        {
            using (StreamReader sr = new StreamReader(cesta))
            {
                string radek;
                while ((radek = sr.ReadLine()) != null)
                {
                    string[] str = radek.Split(' ');
                    Hrac novyHrac = new Hrac(str[0], FotbalovyKlubInfo.DejEnum(str[1]));
                    novyHrac.GolPocet = Convert.ToInt32(str[2]);
                    spojovySeznam.Add(novyHrac);
                }
                OnUpdatedCount();
            }
        }
    }
}
