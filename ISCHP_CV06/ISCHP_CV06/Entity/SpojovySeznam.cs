﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LigaMistru
{
    public class SpojovySeznam : IEnumerable, IEnumerator, ICollection, IList
    {

        private PrvekSeznamu prvni;
        private PrvekSeznamu posledi;
        private int pocetPrvku;

        private int pozice = -1;

        public object this[int index] { get => PrvekVSeznamuOfIndex(index).Data; set => PrvekVSeznamuOfIndex(index).Data = value; }

        public int Count => this.pocetPrvku;

        public object SyncRoot => this;

        public bool IsSynchronized => false;

        public bool IsReadOnly => false;

        public bool IsFixedSize => false;

        public int Add(object value)
        {
            PrvekSeznamu prvek = new PrvekSeznamu(value);
            if (prvni == null && posledi == null)
            {
                prvni = posledi = prvek;
            }
            else if (prvni == posledi)
            {
                prvni.Dalsi = posledi = prvek;
                prvek.Predchozi = prvni;
            }
            else
            {
                posledi.Dalsi = prvek;
                prvek.Predchozi = posledi;
                posledi = prvek;
            }
            pocetPrvku++;
            return Count - 1;
        }

        public void Clear()
        {
            prvni = posledi = null;
            pocetPrvku = 0;
        }

        public bool Contains(object value)
        {
            return IndexOf(value) != -1;
        }

        public void CopyTo(Array array, int index)
        {
            if (Count > 0)
            {
                PrvekSeznamu prvek = prvni;
                for (int i = index; i < Count; i++)
                {
                    if (prvek != null)
                    {
                        array.SetValue(prvek.Data, i - index);
                        prvek = prvek.Dalsi;
                    }
                }
            }
        }

        // IEnumerable
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }

        public bool MoveNext()
        {
            pozice++;
            return (pozice < pocetPrvku);
        }
        public void Reset()
        {
            pozice = -1;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public Object Current
        {
            get
            {
                try
                {
                    return PrvekVSeznamuOfIndex(pozice).Data;
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public int IndexOf(object value)
        {
            int index;
            PrvekVSeznamuOf(value, out index);
            return index;
        }

        public void Insert(int index, object value)
        {

            if (Count > 0)
            {
                PrvekSeznamu prvek = PrvekVSeznamuOfIndex(index);
                PrvekSeznamu prvekKVlozeni = new PrvekSeznamu(value);
                if (prvek != null) {
                    prvekKVlozeni.Predchozi = prvek.Predchozi;
                    if (prvek == prvni)
                    {
                        prvni = prvekKVlozeni;
                    }
                    else
                    {
                        prvek.Predchozi.Dalsi = prvekKVlozeni;
                    }
                    prvekKVlozeni.Dalsi = prvek;
                    prvek.Predchozi = prvekKVlozeni;
                    pocetPrvku++;
                } else {
                    Add(value);
                }          
            }
            else if (index == 0)
            {
                Add(value);
            }
        }

        public void Remove(object value)
        {
            if (Contains(value) && value != null)
            {
                int index;
                PrvekSeznamu prvek = PrvekVSeznamuOf(value, out index);

                //Pokud má předchozí prvek
                if (prvek.Predchozi != null)
                {
                    prvek.Predchozi.Dalsi = prvek.Dalsi;
                }
                else
                {
                    prvni = prvek.Dalsi;
                }

                //Pokud má následníka
                if (prvek.Dalsi != null)
                {
                    prvek.Dalsi.Predchozi = prvek.Predchozi;
                }
                else
                {
                    posledi = prvek.Predchozi;
                }

                //Kontrola, zda-li nedošlo k narušení integrity první/poslední
                if (prvni != null && posledi == null)
                {
                    posledi = prvni;
                }
                else if (prvni == null && posledi != null)
                {
                    prvni = posledi;
                }
                pocetPrvku--;
            }
        }

        public void RemoveAt(int index)
        {
            if (index < pocetPrvku)
                Remove(PrvekVSeznamuOfIndex(index).Data);
        }

        private PrvekSeznamu PrvekVSeznamuOf(object value, out int index)
        {
            if (Count > 0)
            {
                PrvekSeznamu prvek = prvni;
                for (int i = 0; i < Count; i++)
                {
                    if (prvek != null)
                    {
                        if (prvek.Data.Equals(value))
                        {
                            index = i;
                            return prvek;
                        }
                        prvek = prvek.Dalsi;
                    }
                }
            }
            index = -1;
            return null;
        }

        private PrvekSeznamu PrvekVSeznamuOfIndex(int index)
        {

            if (index <= Count - 1)
            {
                PrvekSeznamu prvek = prvni;
                for (int i = 0; i < Count; i++)
                {
                    if (i == index)
                    {
                        return prvek;
                    }
                    prvek = prvek.Dalsi;
                }
            }
            return null;
        }

        private class PrvekSeznamu
        {
            public object Data { get; set; }
            public PrvekSeznamu Dalsi { get; set; }
            public PrvekSeznamu Predchozi { get; set; }

            public PrvekSeznamu(object Data)
            {
                this.Data = Data;
                Dalsi = Predchozi = null;
            }
        }
    }
}
