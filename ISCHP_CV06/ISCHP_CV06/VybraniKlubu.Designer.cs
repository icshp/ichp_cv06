﻿namespace LigaMistru
{
    partial class VybraniKlubu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewKluby = new System.Windows.Forms.ListView();
            this.buttonVybrat = new System.Windows.Forms.Button();
            this.buttonZrusit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listViewKluby
            // 
            this.listViewKluby.HideSelection = false;
            this.listViewKluby.Location = new System.Drawing.Point(12, 12);
            this.listViewKluby.Name = "listViewKluby";
            this.listViewKluby.Size = new System.Drawing.Size(255, 225);
            this.listViewKluby.TabIndex = 0;
            this.listViewKluby.UseCompatibleStateImageBehavior = false;
            this.listViewKluby.View = System.Windows.Forms.View.List;
            // 
            // buttonVybrat
            // 
            this.buttonVybrat.Location = new System.Drawing.Point(11, 256);
            this.buttonVybrat.Name = "buttonVybrat";
            this.buttonVybrat.Size = new System.Drawing.Size(124, 32);
            this.buttonVybrat.TabIndex = 1;
            this.buttonVybrat.Text = "Vybrat";
            this.buttonVybrat.UseVisualStyleBackColor = true;
            this.buttonVybrat.Click += new System.EventHandler(this.buttonVybrat_Click);
            // 
            // buttonZrusit
            // 
            this.buttonZrusit.Location = new System.Drawing.Point(143, 256);
            this.buttonZrusit.Name = "buttonZrusit";
            this.buttonZrusit.Size = new System.Drawing.Size(124, 32);
            this.buttonZrusit.TabIndex = 2;
            this.buttonZrusit.Text = "Zrušit";
            this.buttonZrusit.UseVisualStyleBackColor = true;
            this.buttonZrusit.Click += new System.EventHandler(this.buttonZrusit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 240);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Držte ctrl + klikejte na požadované kluby";
            // 
            // VybraniKlubu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(279, 300);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonZrusit);
            this.Controls.Add(this.buttonVybrat);
            this.Controls.Add(this.listViewKluby);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VybraniKlubu";
            this.Text = "Volba klubu k exportu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewKluby;
        private System.Windows.Forms.Button buttonVybrat;
        private System.Windows.Forms.Button buttonZrusit;
        private System.Windows.Forms.Label label1;
    }
}