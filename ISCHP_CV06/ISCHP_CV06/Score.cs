﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LigaMistru
{
    public partial class Score : Form
    {
        public Score(Hraci hraci)
        {
            InitializeComponent();

            hraci.NajdiNejlepsiKluby(out FotbalovyKlub[] fk, out int pocet);
            textBox1.Text = pocet.ToString();
            foreach (var item in fk)
            {
                listBoxKluby.Items.Add(item);
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
