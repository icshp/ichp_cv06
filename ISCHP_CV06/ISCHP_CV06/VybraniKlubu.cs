﻿using LigaMistru;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LigaMistru
{
    public partial class VybraniKlubu : Form
    {
        public List<FotbalovyKlub> zvoleneKluby { get; set; }

        public VybraniKlubu(FotbalovyKlub[] fk)
        {
            InitializeComponent();
            zvoleneKluby = new List<FotbalovyKlub>();
            foreach (var item in fk)
            {
                listViewKluby.Items.Add(item.ToString());
            }
        }

        private void buttonVybrat_Click(object sender, EventArgs e)
        {
            
            foreach (ListViewItem item in listViewKluby.SelectedItems)
            {
                zvoleneKluby.Add(FotbalovyKlubInfo.DejEnum(item.Text));
            }
            if (zvoleneKluby.Count != 0)
            {
                this.DialogResult = DialogResult.OK;
            }
            else {
                this.DialogResult = DialogResult.Abort;
            }
            this.Close();
        }

        private void buttonZrusit_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
