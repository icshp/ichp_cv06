﻿namespace LigaMistru
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.labelState = new System.Windows.Forms.ToolStripStatusLabel();
            this.listBoxPocetHracu = new System.Windows.Forms.ListBox();
            this.buttonKonec = new System.Windows.Forms.Button();
            this.buttonZrusit = new System.Windows.Forms.Button();
            this.buttonRegistrovat = new System.Windows.Forms.Button();
            this.buttonNejKlub = new System.Windows.Forms.Button();
            this.buttonUprav = new System.Windows.Forms.Button();
            this.buttonVymaz = new System.Windows.Forms.Button();
            this.buttonPridejHrace = new System.Windows.Forms.Button();
            this.listBoxHraci = new System.Windows.Forms.ListBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.labelState});
            this.statusStrip1.Location = new System.Drawing.Point(0, 323);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(647, 22);
            this.statusStrip1.TabIndex = 19;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(50, 17);
            this.toolStripStatusLabel1.Text = "Výpis je:";
            // 
            // labelState
            // 
            this.labelState.ForeColor = System.Drawing.Color.Red;
            this.labelState.Name = "labelState";
            this.labelState.Size = new System.Drawing.Size(157, 17);
            this.labelState.Text = "Vypnutý! (Registruj handler!)";
            // 
            // listBoxPocetHracu
            // 
            this.listBoxPocetHracu.FormattingEnabled = true;
            this.listBoxPocetHracu.Location = new System.Drawing.Point(10, 215);
            this.listBoxPocetHracu.Name = "listBoxPocetHracu";
            this.listBoxPocetHracu.Size = new System.Drawing.Size(516, 95);
            this.listBoxPocetHracu.TabIndex = 18;
            // 
            // buttonKonec
            // 
            this.buttonKonec.Location = new System.Drawing.Point(535, 243);
            this.buttonKonec.Name = "buttonKonec";
            this.buttonKonec.Size = new System.Drawing.Size(102, 23);
            this.buttonKonec.TabIndex = 17;
            this.buttonKonec.Text = "Konec";
            this.buttonKonec.UseVisualStyleBackColor = true;
            this.buttonKonec.Click += new System.EventHandler(this.buttonKonec_Click);
            // 
            // buttonZrusit
            // 
            this.buttonZrusit.Location = new System.Drawing.Point(535, 156);
            this.buttonZrusit.Name = "buttonZrusit";
            this.buttonZrusit.Size = new System.Drawing.Size(102, 23);
            this.buttonZrusit.TabIndex = 16;
            this.buttonZrusit.Text = "Zrušit registraci";
            this.buttonZrusit.UseVisualStyleBackColor = true;
            this.buttonZrusit.Click += new System.EventHandler(this.buttonZrusit_Click);
            // 
            // buttonRegistrovat
            // 
            this.buttonRegistrovat.Location = new System.Drawing.Point(535, 128);
            this.buttonRegistrovat.Name = "buttonRegistrovat";
            this.buttonRegistrovat.Size = new System.Drawing.Size(102, 23);
            this.buttonRegistrovat.TabIndex = 15;
            this.buttonRegistrovat.Text = "Registrovat\r\n";
            this.buttonRegistrovat.UseVisualStyleBackColor = true;
            this.buttonRegistrovat.Click += new System.EventHandler(this.buttonRegistrovat_Click);
            // 
            // buttonNejKlub
            // 
            this.buttonNejKlub.Location = new System.Drawing.Point(535, 98);
            this.buttonNejKlub.Name = "buttonNejKlub";
            this.buttonNejKlub.Size = new System.Drawing.Size(102, 23);
            this.buttonNejKlub.TabIndex = 14;
            this.buttonNejKlub.Text = "Nejlepší klub";
            this.buttonNejKlub.UseVisualStyleBackColor = true;
            this.buttonNejKlub.Click += new System.EventHandler(this.buttonNejKlub_Click);
            // 
            // buttonUprav
            // 
            this.buttonUprav.Location = new System.Drawing.Point(535, 69);
            this.buttonUprav.Name = "buttonUprav";
            this.buttonUprav.Size = new System.Drawing.Size(102, 23);
            this.buttonUprav.TabIndex = 13;
            this.buttonUprav.Text = "Uprav hráče";
            this.buttonUprav.UseVisualStyleBackColor = true;
            this.buttonUprav.Click += new System.EventHandler(this.buttonUprav_Click);
            // 
            // buttonVymaz
            // 
            this.buttonVymaz.Location = new System.Drawing.Point(535, 41);
            this.buttonVymaz.Name = "buttonVymaz";
            this.buttonVymaz.Size = new System.Drawing.Size(102, 23);
            this.buttonVymaz.TabIndex = 12;
            this.buttonVymaz.Text = "Vymaž hráče";
            this.buttonVymaz.UseVisualStyleBackColor = true;
            this.buttonVymaz.Click += new System.EventHandler(this.buttonVymaz_Click);
            // 
            // buttonPridejHrace
            // 
            this.buttonPridejHrace.Location = new System.Drawing.Point(535, 11);
            this.buttonPridejHrace.Name = "buttonPridejHrace";
            this.buttonPridejHrace.Size = new System.Drawing.Size(102, 23);
            this.buttonPridejHrace.TabIndex = 11;
            this.buttonPridejHrace.Text = "Přidat hráče";
            this.buttonPridejHrace.UseVisualStyleBackColor = true;
            this.buttonPridejHrace.Click += new System.EventHandler(this.buttonPridejHrace_Click);
            // 
            // listBoxHraci
            // 
            this.listBoxHraci.FormattingEnabled = true;
            this.listBoxHraci.Location = new System.Drawing.Point(10, 11);
            this.listBoxHraci.Name = "listBoxHraci";
            this.listBoxHraci.Size = new System.Drawing.Size(516, 199);
            this.listBoxHraci.TabIndex = 10;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(535, 185);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(102, 23);
            this.buttonSave.TabIndex = 20;
            this.buttonSave.Text = "Uložit hráče";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Location = new System.Drawing.Point(535, 214);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(102, 23);
            this.buttonLoad.TabIndex = 21;
            this.buttonLoad.Text = "Načíst hráče";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.FileName = "export";
            this.saveFileDialog.Filter = "txt files (*.txt)|*.txt";
            this.saveFileDialog.RestoreDirectory = true;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "export.txt";
            this.openFileDialog.Filter = "txt files (*.txt)|*.txt";
            this.openFileDialog.RestoreDirectory = true;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 345);
            this.Controls.Add(this.buttonLoad);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.listBoxPocetHracu);
            this.Controls.Add(this.buttonKonec);
            this.Controls.Add(this.buttonZrusit);
            this.Controls.Add(this.buttonRegistrovat);
            this.Controls.Add(this.buttonNejKlub);
            this.Controls.Add(this.buttonUprav);
            this.Controls.Add(this.buttonVymaz);
            this.Controls.Add(this.buttonPridejHrace);
            this.Controls.Add(this.listBoxHraci);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.Text = "Liga mistrů";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel labelState;
        private System.Windows.Forms.ListBox listBoxPocetHracu;
        private System.Windows.Forms.Button buttonKonec;
        private System.Windows.Forms.Button buttonZrusit;
        private System.Windows.Forms.Button buttonRegistrovat;
        private System.Windows.Forms.Button buttonNejKlub;
        private System.Windows.Forms.Button buttonUprav;
        private System.Windows.Forms.Button buttonVymaz;
        private System.Windows.Forms.Button buttonPridejHrace;
        private System.Windows.Forms.ListBox listBoxHraci;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}

