﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LigaMistru;

namespace LigaMistru.Tests
{
    [TestClass]
    public class FotbalovyKlubInfoTests
    {
        [TestMethod]
        public void DejNazev()
        {
            Assert.AreEqual("Real Madrid", FotbalovyKlubInfo.DejNazev(FotbalovyKlub.RealMadrid));
            Assert.AreEqual("FC Porto", FotbalovyKlubInfo.DejNazev(FotbalovyKlub.FCPorto));
            Assert.AreEqual("Nemá klub", FotbalovyKlubInfo.DejNazev(FotbalovyKlub.None));
            Assert.AreEqual("Arsenal", FotbalovyKlubInfo.DejNazev(FotbalovyKlub.Arsenal));
        }

        [TestMethod]
        public void ToEnum()
        {
            foreach (FotbalovyKlub klub in FotbalovyKlubInfo.ZiskejKluby())
            {
                Assert.AreEqual(klub, FotbalovyKlubInfo.DejEnum(klub.ToString()));
            }
        }

        [TestMethod]
        public void ziskejPocet()
        {
            Assert.AreEqual(6, FotbalovyKlubInfo.Pocet);
        }




    }
}
